# Напишите программу которая удаляет пробел в начале, в конце строки

str_1 = ' Hello World! '
print(str_1 + 'str_len: ' + str(len(str_1)))

str_1 = str_1.split()
str_1 = ' '.join(str_1)

print(str_1 + ' str_len: ' + str(len(str_1)))
